package main

import (
	"esusu-memes-as-a-service/internal/logging"
	"esusu-memes-as-a-service/internal/serve"
	"esusu-memes-as-a-service/internal/storage"
	"fmt"
	"net/http"
	"os"
)

var (
	logger = logging.GetInstance()
)

func main() {
	logger.Info("Starting service")

	port := os.Getenv("PORT")

	db, err := storage.NewDb(
		storage.DbConfig{
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			Database: os.Getenv("DB_NAME"),
		},
	)

	if err != nil {
		logger.Error("err", err)

		os.Exit(1)
	}

	maasDb, err := storage.NewMaasDb(db)

	if err != nil {
		logger.Error("Failed to setup database", "err", err)

		os.Exit(1)
	}

	handlerCtx := &serve.HandlerContext{
		MaasDb: maasDb,
	}

	mux := serve.NewMemeMux(handlerCtx)

	addr := fmt.Sprintf(":%s", port)

	err = http.ListenAndServe(addr, mux)

	if err != nil {
		logger.Error("Failed to start server", "err", err)
	}
}
