FROM golang:1.21 as prep

RUN go install github.com/go-delve/delve/cmd/dlv@v1.22.1

WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download
COPY cmd cmd
COPY internal internal

FROM prep as debug

ENV GOTRACEBACK=all
RUN go build -gcflags='all=-N -l' -v -o /app cmd/main.go
CMD [ "/go/bin/dlv", "--listen=:56268", "--headless=true", "--api-version=2", "--accept-multiclient", "exec", "/app"]

FROM prep as build

RUN CGO_ENABLED=0 go build -v -o /app cmd/main.go

FROM gcr.io/distroless/base-debian12 as final

COPY --from=build /app /
CMD ["/app"]
