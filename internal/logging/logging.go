package logging

import (
	"log"
	"log/slog"
	"os"
	"sync"
)

var (
	initLogging sync.Once
	logger      *slog.Logger
)

func GetInstance() *slog.Logger {
	initLogging.Do(func() {
		logLevel := os.Getenv("LOG_LEVEL")

		lvlVar := new(slog.LevelVar)
		err := lvlVar.UnmarshalText([]byte(logLevel))

		if err != nil {
			log.Fatalln(err.Error())
		}

		logger = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: lvlVar,
		}))
	})

	return logger
}
