package storage

func (m *MaasDb) createAccountsTable() error {
	_, err := m.db.Exec("CREATE TABLE IF NOT EXISTS accounts (accountId VARCHAR(256) NOT NULL PRIMARY KEY, tokens INT)")

	if err != nil {
		return err
	}

	return nil
}

func (m *MaasDb) UpdateTokenCount(accountId string, tokenCount int) error {
	_, err := m.db.Exec("UPDATE accounts SET tokens = (?) WHERE accountId = (?)", tokenCount, accountId)

	if err != nil {
		return err
	}

	return nil
}

func (m *MaasDb) GetTokenCount(accountId string) (int, error) {
	row := m.db.QueryRow("SELECT tokens FROM accounts WHERE accountId = (?)", accountId)

	var tokenCount int

	err := row.Scan(&tokenCount)

	if err != nil {
		return 0, err
	}

	return tokenCount, nil
}
