package storage

import (
	"database/sql"
	"esusu-memes-as-a-service/internal/logging"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var (
	logger = logging.GetInstance()
)

type DbConfig struct {
	Database string
	Host     string
	Password string
	Port     string
	User     string
}

// MaasDb Contains the configured database to use with Memes-as-a-Service
type MaasDb struct {
	db *sql.DB
}

// NewDb Create a new database connection using the given configuration
func NewDb(config DbConfig) (*sql.DB, error) {
	dataSrc := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.User, config.Password, config.Host, config.Port, config.Database)
	db, err := sql.Open("mysql", dataSrc)

	if err != nil {
		logger.Error("Failed to connect to database %s", config.Database)

		return nil, err
	}

	return db, nil
}

// NewMaasDb Set up the Memes-as-a-Service database
func NewMaasDb(db *sql.DB) (*MaasDb, error) {
	maasDb := &MaasDb{
		db: db,
	}

	err := maasDb.createAccountsTable()

	if err != nil {
		return nil, err
	}

	return maasDb, nil
}
