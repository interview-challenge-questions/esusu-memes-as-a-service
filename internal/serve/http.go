package serve

import (
	"database/sql"
	"errors"
	"esusu-memes-as-a-service/internal/logging"
	"esusu-memes-as-a-service/internal/storage"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

var (
	logger = logging.GetInstance()
)

type HandlerContext struct {
	MaasDb *storage.MaasDb
}

func NewMemeMux(handlerCtx *HandlerContext) *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/memes", handlerCtx.bearerAuth(handlerCtx.getMemesHandler))

	return mux
}

func (m *HandlerContext) getMemesHandler(w http.ResponseWriter, r *http.Request) {
	params, _ := url.Parse(r.URL.String())
	q := params.Query()

	lat := q.Get("lat")

	if len(lat) == 0 {
		lat = "0.0"
	}

	lon := q.Get("lon")

	if len(lon) == 0 {
		lon = "0.0"
	}

	memeQuery := q.Get("query")

	if len(memeQuery) == 0 {
		memeQuery = "some-random-meme"
	}

	fmt.Fprintf(w, "Here is a meme for lat %s lon %s with query %s\n", lat, lon, memeQuery)
}

// bearerAuth Wraps around functions to handle bearer authorization. Decrements an accounts tokens
// upon successful serving of meme.
func (m *HandlerContext) bearerAuth(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.Info("Authorizing request", "path", r.URL.Path)

		accountId, err := getAccountId(r.Header)

		if err != nil {
			http.Error(w, "Bad request", http.StatusBadRequest)

			return
		}

		tokenCount, err := m.MaasDb.GetTokenCount(accountId)

		if errors.Is(err, sql.ErrNoRows) {
			http.Error(w, "Account ID does not exist", http.StatusNotFound)

			return
		} else if err != nil {
			http.Error(w, "Unknown error", http.StatusInternalServerError)

			logger.Error("Failed to get token count", "error", err)

			return
		} else if tokenCount < 1 {
			http.Error(w, "No tokens remaining", http.StatusUnauthorized)

			return
		}

		handlerFunc.ServeHTTP(w, r)

		err = m.MaasDb.UpdateTokenCount(accountId, tokenCount-1)

		if err != nil {
			logger.Error("Failed to update token count", "err", err)
		}
	}
}

// getAccountId Get the account ID from the authorization header.
func getAccountId(header http.Header) (string, error) {
	scheme, accountId, _ := strings.Cut(header.Get("Authorization"), " ")

	if scheme != "Bearer" {
		return accountId, fmt.Errorf("invalid authorization header")
	}

	return accountId, nil
}
