# memes-as-a-service

## Overview

This repo contains the completed project and answered questions
for `Domain Challenge: Credit Reporting`.

Information on how to run the application for questions 1 and 2 is given below. Answers for
questions 3 and 4 are also in this repo and can be found [here](docs/answered-questions.md).

## Requirements

- Kubernetes
- Helm
- Skaffold
- Curl

## Setup

These steps describe how to setup the application from the root of this repository.

1) Create a MariaDB instance using `helm`:

```shell
helm install mariadb oci://registry-1.docker.io/bitnamicharts/mariadb --values configs/mariadb-values.yaml
```

Until the team responsible for managing token purchases implements that functionality, tokens must
be added manually to this database. Credential information for this database instance can be found
[here](configs/mariadb-values.yaml).

2) Run the application using `skaffold`:

```shell
skaffold dev --port-forward
```

This deploys the application's [Helm chart](deploy/memes-as-a-service) using
the [default configuration](deploy/memes-as-a-service/values.yaml).

## Run

Now that the application stack has been setup and deployed, we can make a request:

```shell
curl -H "Authorization: Bearer 1234" 'localhost:8080/memes?lat=40.730610&lon=-73.935242&query=food'
```
