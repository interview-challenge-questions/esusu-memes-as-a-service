3. **Explain in as much detail as you like how you would scale this API service to ultimately
   support a volume of 10,000 requests per second. Some things to consider include:**
    * **How will you handle CI/CD in the context of a live service?**\
      CI/CD should take care of building and distributing artifacts, running tests (unit,
      integration, and end-to-end), code quality and security scanning, and deployments to test
      environments and prod, all of which should be automated based on repository activity (e.g. git
      push/tag). For deploys to prod specifically, a canary deployment (i.e. a deployment to a very
      small area) would be useful to minimize the impact of any errors that may occur. Once the
      canary deploy has shown to be successful, the application can then be rolled out to the rest
      of prod.
    * **How will you model and support SLAs? What should operational SLAs be for this
      service?**\
      SLAs could be modeled and supported with the use of observability tools. For example, the
      application could export Prometheus metrics that track how many requests have been made and
      how long they take. These would then be scraped by a Prometheus server and used to model SLAs
      that would need to be implemented. Operational SLAs could monitor how long it takes for
      testing to complete and how long deploys to different environments take so that expectations
      can be set for delivery of the service.
    * **How do you support geographically diverse clients? As you scale the system out
      horizontally, how do you continue to keep track of tokens without slowing down
      the system?**\
      Depending on the requirements for this service, a single data center with multiple nodes for
      compute and storage may be acceptable. However, this would add latency for clients the further
      they are from the data center. If this latency is not acceptable, the system could be scaled
      out across multiple data centers so that they are closer to the clients. This approach would
      add complexity to the system because data must be synchronized in some way across all the data
      centers. One possible way to do this would be to replicate data updates to each of the data
      centers using a message broker (e.g. Kafka). In order to keep track of the tokens while
      ensuring the system does not slow down, the data must be divided into smaller pieces for
      processing (i.e. sharded or partitioned). This could be done based on the location of the
      client.
4. **The success of this product has led to the team adding new and exciting features which
   the customers have highly requested. Specifically, we are now offering a premium
   offering: Memes AI. With Memes AI, you can get even spicier memes curated by
   generative AI. Naturally, this feature costs extra money and requires a separate
   subscription.\
   Describe how you would modify the service to now keep track of whether a client is authorized
   to get AI-generated memes. If a client has this subscription, then they should get AI-memes,
   and they should get normal memes otherwise. How do you keep track of authorization of a
   client as we scale the system without slowing down performance?**\
   Instead of using a crude database query to manage user authorization, we could update the service
   to utilize OAuth. This would allow for authentication to have scopes of access for the AI
   generated memes and move authentication to a separate service to scale horizontally allowing for
   more users. It also has the benefit of easier integration with other applications in the future
   because it's an industry standard.
